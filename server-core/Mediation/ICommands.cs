using System.Threading.Tasks;

namespace Solera.Server.Core.Mediation
{
    public interface ICommands
    {
        Task Execute<TCommand>(TCommand command) where TCommand : ICommand;
    }
}