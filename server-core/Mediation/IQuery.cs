using MediatR;

namespace Solera.Server.Core.Mediation
{
    public interface IQuery<TResult> : IRequest<TResult> { }
}