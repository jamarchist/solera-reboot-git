using MediatR;

namespace Solera.Server.Core.Mediation
{
    public interface IAsyncQueryHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> where TRequest : IQuery<TResponse> { }
}