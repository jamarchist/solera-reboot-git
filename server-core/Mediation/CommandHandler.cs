using MediatR;

namespace Solera.Server.Core.Mediation
{
    public abstract class CommandHandler<TRequest> : RequestHandler<TRequest> where TRequest : ICommand { }
}