using MediatR;

namespace Solera.Server.Core.Mediation
{
    public interface ICommand : IRequest { }
}