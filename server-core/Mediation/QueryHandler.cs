using MediatR;

namespace Solera.Server.Core.Mediation
{
    public abstract class QueryHandler<TRequest, TResponse> : RequestHandler<TRequest, TResponse> where TRequest : IQuery<TResponse> { }
}