using System.Threading.Tasks;
using MediatR;

namespace Solera.Server.Core.Mediation
{
    public interface IQueries
    {
        Task<TResult> Query<TResult>(IRequest<TResult> query);
    }
}