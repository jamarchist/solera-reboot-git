using MediatR;

namespace Solera.Server.Core.Mediation
{
    public abstract class AsyncCommandHandler<TRequest> : AsyncRequestHandler<TRequest> where TRequest : ICommand { }
}