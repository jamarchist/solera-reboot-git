using System.Threading.Tasks;
using MediatR;

namespace Solera.Server.Core.Mediation
{
    public class MediatRExecutor : IExecutor
    {
        private readonly IMediator mediator;

        public MediatRExecutor(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public Task Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            return mediator.Send(command);
        }

        public Task<TResult> Query<TResult>(IRequest<TResult> query)
        {
            return mediator.Send(query);
        }
    }
}