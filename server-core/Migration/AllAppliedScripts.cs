using System.Collections.Generic;
using MongoDB.Driver;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Mongo;

namespace Solera.Server.Core.Migration
{
    public class AllAppliedScripts : IQuery<IReadOnlyList<DatabaseVersion>> { }

    public class AllAppliedScriptsHandler : QueryHandler<AllAppliedScripts, IReadOnlyList<DatabaseVersion>>
    {
        private readonly IDatabase db;

        public AllAppliedScriptsHandler(IDatabase db)
        {
            this.db = db;
        }

        protected override IReadOnlyList<DatabaseVersion> Handle(AllAppliedScripts message)
        {
            var applied = db.GetCollection<DatabaseVersion>().Find(v => true).ToList();
            return applied;
        }
    }
}