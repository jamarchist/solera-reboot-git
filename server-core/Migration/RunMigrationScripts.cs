using System;
using System.Collections.Generic;
using System.Linq;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Mongo;

namespace Solera.Server.Core.Migration
{
    public class RunMigrationScripts : ICommand
    {
        public IReadOnlyList<Type> ScriptTypes { get; set; }
    }

    public class RunMigrationScriptsHandler : CommandHandler<RunMigrationScripts>
    {
        private readonly IExecutor mediator;
        private readonly IDatabase db;

        public RunMigrationScriptsHandler(IExecutor mediator, IDatabase db)
        {
            this.mediator = mediator;
            this.db = db;
        }

        protected override void Handle(RunMigrationScripts message)
        {
            var now = DateTime.UtcNow;
            var appliedVersions = db.GetCollection<DatabaseVersion>();
            var commands = message.ScriptTypes.Select(t => new { Instance = Activator.CreateInstance(t) as ICommand, Info = t }).ToList();
            foreach (var command in commands)
            {
                mediator.Execute(command.Instance);
                appliedVersions.InsertOne(new DatabaseVersion
                {
                    ScriptName = command.Info.FullName,
                    DateAppliedUtc = now
                });
            }
        }
    }
}