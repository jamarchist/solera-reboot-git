using System;
using System.Collections.Generic;
using System.Linq;
using Solera.Server.Core.Mediation;
using Serilog;

namespace Solera.Server.Core.Migration
{
    public class AllPotentialScripts : IQuery<IReadOnlyList<Type>> { }

    public class AllPotentialScriptsHandler : QueryHandler<AllPotentialScripts, IReadOnlyList<Type>>
    {
        protected override IReadOnlyList<Type> Handle(AllPotentialScripts message)
        {
            var scriptsNamespace = typeof(Solera.Server.Core.MigrationScripts.Types).Namespace;
            var scriptsAssembly = typeof(Solera.Server.Core.MigrationScripts.Types).Assembly;
            var command = typeof(ICommand);

            var scripts = scriptsAssembly.GetTypes()
                .Where(t => t.Namespace != null)
                .Where(t => t.Namespace.StartsWith(scriptsNamespace))
                .Where(t => command.IsAssignableFrom(t))
                    .ToList();
            return scripts;
        }
    }
}