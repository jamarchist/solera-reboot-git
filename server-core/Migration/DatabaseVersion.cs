using System;
using Solera.Server.Core.Mongo;

namespace Solera.Server.Core.Migration
{
    public class DatabaseVersion : BaseDocument
    {
        public string ScriptName { get; set; }
        public DateTime DateAppliedUtc { get; set; }
    }
}