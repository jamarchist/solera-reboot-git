using Solera.Server.Core.Mediation;
using Serilog;

namespace Solera.Server.Core.Migration
{
    public class MigrateDatabase : ICommand { }

    public class MigrateDatabaseHandler : CommandHandler<MigrateDatabase>
    {
        private readonly IExecutor mediator;
        private readonly ILogger log;

        public MigrateDatabaseHandler(IExecutor mediator, ILogger log)
        {
            this.mediator = mediator;
            this.log = log;
        }

        protected override void Handle(MigrateDatabase message)
        {
            var scripts = mediator.Query(new AllPendingScripts()).Result;
            log.Information($"There are {scripts.Count} pending scripts.");
            mediator.Execute(new RunMigrationScripts { ScriptTypes = scripts });
        }
    }
}