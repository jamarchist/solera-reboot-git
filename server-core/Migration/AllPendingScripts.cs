using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Solera.Server.Core.Mediation;
using Serilog;

namespace Solera.Server.Core.Migration
{
    public class AllPendingScripts : IQuery<IReadOnlyList<Type>> { }

    public class AllPendingScriptsHandler : IAsyncQueryHandler<AllPendingScripts, IReadOnlyList<Type>>
    {
        private readonly IExecutor mediator;
        
        public AllPendingScriptsHandler(IExecutor mediator)
        {
            this.mediator = mediator;
        }

        public async Task<IReadOnlyList<Type>> Handle(AllPendingScripts request, CancellationToken cancellationToken)
        {
            var applied = await mediator.Query(new AllAppliedScripts());
            var potential = await mediator.Query(new AllPotentialScripts());

            var appliedScripts = applied.Select(a => a.ScriptName).ToList();
            var pending = potential.Where(p => !appliedScripts.Contains(p.FullName)).ToList();
            
            return pending;
        }
    }
}