using Nancy;
using Nancy.Bootstrapper;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Migration;
using Serilog;

namespace Solera.Server.Core.Startup
{
    public class MigrateDatabaseOnStartup : IApplicationStartup
    {
        private readonly ICommands commands;
        private readonly ILogger log;

        public MigrateDatabaseOnStartup(ICommands commands, ILogger log)
        {
            this.commands = commands;
            this.log = log;
        }

        public void Initialize(IPipelines pipelines)
        {
            log.Information("Initializing application. MigrateDatabaseOnStartup");
            commands.Execute(new MigrateDatabase()).Wait();
        }
    }
}