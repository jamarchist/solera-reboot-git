using Nancy;

namespace Solera.Server.Core.Routes.Vue
{
    public class AdminRoute : NancyModule 
    {
        public AdminRoute()
        {
            Get("/admin", async args => await View["admin-dist/index.html"]);
        }
    }
}