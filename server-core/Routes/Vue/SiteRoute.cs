using Nancy;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Logging;


namespace Solera.Server.Core.Routes.Vue
{
    public class SiteRoute : NancyModule
    {
        public SiteRoute(IExecutor mediator)
        {
            Get("/", async args =>
            {
                // This is just to test out infrastructure with deployment
                // I should do some writing 'OnError'...
                mediator.Execute(new LogStartup()).Wait();
                await View["site-dist/index.html"];
            });
        }
    }
}