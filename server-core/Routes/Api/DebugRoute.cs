using System.Linq;
using Microsoft.Extensions.Configuration;
using Nancy;
using Solera.Server.Core.Logging;

namespace Solera.Server.Core.Api
{
    public class DebugRoute : NancyModule
    {
        public DebugRoute(IConfiguration config, MemoryLog log)
        {
            Get("/debug", async args => 
            {
                //var configValues = config.AsEnumerable().ToDictionary(cv => cv.Key, cv => cv.Value);
                return Response.AsJson(new 
                {
                    LogMessages = log.Messages
                });
            });
        }
    }
}