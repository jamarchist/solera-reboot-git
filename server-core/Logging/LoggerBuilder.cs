using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole;
using Microsoft.Extensions.Configuration;

namespace Solera.Server.Core.Logging
{
    public class LoggerBuilder
    {
        private readonly MemoryLog memoryLog;

        public LoggerBuilder(MemoryLog memoryLog)
        {
            this.memoryLog = memoryLog;
        }

        public ILogger Build()
        {
            return new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.Trace()
                .WriteTo.Sink(new MemorySink(memoryLog))
                    .CreateLogger();
        }
    }
}