using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Solera.Server.Core.Configuration;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Mongo;
using Solera.Server.Core.Migration;
using MongoDB.Driver;
using Serilog;

namespace Solera.Server.Core.Logging
{
    public class LogStartup : ICommand { }

    public class LogStartupHandler : CommandHandler<LogStartup>
    {
        private readonly IDatabase db;
        private readonly ILogger log;

        public LogStartupHandler(IDatabase db, ILogger log)
        {
            this.db = db;
            this.log = log;
        }

        protected override void Handle(LogStartup request)
        {
            var versions = db.GetCollection<DatabaseVersion>().Find(x => true).ToList();
            log.Information($"There are {versions.Count} versions.");
        }
    }
}