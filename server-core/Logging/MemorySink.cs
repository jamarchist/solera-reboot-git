using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Solera.Server.Core.Logging
{
    public class MemorySink : ILogEventSink
    {
        private readonly MemoryLog log;

        public MemorySink(MemoryLog log)
        {
            this.log = log;
        }

        public void Emit(LogEvent logEvent)
        {
            log.Messages.Add($"[{logEvent.Level}] {logEvent.RenderMessage()}");
        }
    }
}