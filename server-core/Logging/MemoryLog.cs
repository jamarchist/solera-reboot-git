using System.Collections.Generic;

namespace Solera.Server.Core.Logging
{
    public class MemoryLog
    {
        private readonly IList<string> messages = new List<string>();

        public IList<string> Messages { get { return messages; } }
    }
}