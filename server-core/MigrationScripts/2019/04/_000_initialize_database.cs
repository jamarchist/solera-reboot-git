using Solera.Server.Core.Mediation;
using Serilog;

namespace Solera.Server.Core.MigrationScripts._2019._04
{
    public class _000_initialize_database : ICommand { }

    public class _000_initialize_database_handler : CommandHandler<_000_initialize_database>
    {
        private readonly ILogger log;

        public _000_initialize_database_handler(ILogger log)
        {
            this.log = log;
        }

        protected override void Handle(_000_initialize_database request)
        {
            // This is just to test the database versioning
            log.Information("_000_initialize_database executed.");
        }
    }
}