using Autofac;
using Solera.Server.Core.Configuration;

namespace Solera.Server.Core.Composition
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterSource(new ConfigurationSource());
        }
    }
}