using System;
using Autofac;
using MongoDB.Driver;
using Solera.Server.Core.Configuration;
using Solera.Server.Core.Mongo;

namespace Solera.Server.Core.Composition
{
    public class MongoModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c =>
            {
                var config = c.Resolve<ConnectionStringsSettings>();
                return new MongoUrl(config.MongoDB);
            }).As<MongoUrl>().SingleInstance();

            builder.Register(c =>
            {
                var url = c.Resolve<MongoUrl>();
                return new MongoClient(url);
            }).As<IMongoClient>().SingleInstance();

            builder.Register<Func<string, IMongoDatabase>>(c =>
            {
                var client = c.Resolve<IMongoClient>();
                return name => client.GetDatabase(name);
            });

            builder.Register(c =>
            {
                var url = c.Resolve<MongoUrl>();
                var dbFactory = c.Resolve<Func<string, IMongoDatabase>>();

                return dbFactory(url.DatabaseName);
            }).As<IMongoDatabase>();

            builder.RegisterType<PluralizedCollectionsMongoDatabase>().As<IDatabase>();
        }
    }
}