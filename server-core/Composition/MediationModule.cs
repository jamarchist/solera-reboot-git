using Autofac;
using MediatR;
using Solera.Server.Core.Mediation;

namespace Solera.Server.Core.Composition
{
    public class MediationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();
            builder.RegisterType<MediatRExecutor>()
                .As<IExecutor>()
                .As<ICommands>()
                .As<IQueries>();

            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            builder.RegisterMediationHandlers(ThisAssembly);
        }
    }
}