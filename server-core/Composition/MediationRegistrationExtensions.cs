using Autofac;
using Solera.Server.Core.Mediation;

namespace Solera.Server.Core.Composition
{
    public static class MediationRegistrationExtensions
    {
        public static void RegisterMediationHandlers(this ContainerBuilder builder, params System.Reflection.Assembly[] assemblies)
        {
            foreach (var assembly in assemblies)
            {
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.IsClosedTypeOf(typeof(AsyncCommandHandler<>)) ||
                                t.IsClosedTypeOf(typeof(IAsyncQueryHandler<,>)) ||
                                t.IsClosedTypeOf(typeof(CommandHandler<>)) ||
                                t.IsClosedTypeOf(typeof(QueryHandler<,>)))
                    .AsImplementedInterfaces();
            }
        }
    }
}