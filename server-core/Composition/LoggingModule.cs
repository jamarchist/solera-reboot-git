using Autofac;
using Serilog;
using Solera.Server.Core.Logging;

namespace Solera.Server.Core.Composition
{
    public class LoggingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MemoryLog>().AsSelf().SingleInstance();
            builder.RegisterType<LoggerBuilder>().AsSelf();
            builder.Register(c => 
            {
                var logBuilder = c.Resolve<LoggerBuilder>();
                Log.Logger = logBuilder.Build();

                return Log.Logger;
            }).As<ILogger>().SingleInstance();
        }
    }
}