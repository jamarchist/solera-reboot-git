using MongoDB.Bson;

namespace Solera.Server.Core.Mongo
{

    public abstract class BaseDocument : IBaseDocument
    {
        public ObjectId Id { get; set;}
    }
}