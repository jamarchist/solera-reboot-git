using MongoDB.Bson;

namespace Solera.Server.Core.Mongo
{
    public interface IBaseDocument
    {
        ObjectId Id { get; }
    }
}