using MongoDB.Driver;
using Pluralize.NET.Core;

namespace Solera.Server.Core.Mongo
{
    public class PluralizedCollectionsMongoDatabase : IDatabase
    {
        private readonly IMongoDatabase mongo;
        private readonly Pluralizer pluralizer;

        public PluralizedCollectionsMongoDatabase(IMongoDatabase mongo)
        {
            this.mongo = mongo;
            this.pluralizer = new Pluralizer();
        }

        public void DropCollection<T>()
        {
            mongo.DropCollection(Pluralize<T>());
        }

        public IMongoCollection<T> GetCollection<T>()
        {
            return mongo.GetCollection<T>(Pluralize<T>());
        }

        private string Pluralize<T>()
        {
            return pluralizer.Pluralize(typeof(T).Name);
        }        
    }
}