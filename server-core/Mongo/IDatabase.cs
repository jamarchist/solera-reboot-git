using MongoDB.Driver;

namespace Solera.Server.Core.Mongo
{
    public interface IDatabase
    {
        IMongoCollection<T> GetCollection<T>();
        void DropCollection<T>();        
    }
}