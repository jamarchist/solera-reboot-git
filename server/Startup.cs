﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Nancy.Owin;
using Serilog;
using Serilog.AspNetCore;

namespace Solera.Server
{
    public class Startup
    {
        private ILifetimeScope container;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule<Registrations>();

            container = containerBuilder.Build();

            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ConfigureStaticDirectoriesFor("admin-dist", "css", "/admin/css", app);
            ConfigureStaticDirectoriesFor("admin-dist", "img", "/admin/img", app);
            ConfigureStaticDirectoriesFor("admin-dist", "js", "/admin/js", app);
            ConfigureStaticDirectoriesFor("admin-dist", "fonts", "/admin/fonts", app);

            ConfigureStaticDirectoriesFor("site-dist", "css", "/css", app);
            ConfigureStaticDirectoriesFor("site-dist", "img", "/img", app);
            ConfigureStaticDirectoriesFor("site-dist", "js", "/js", app);
            ConfigureStaticDirectoriesFor("site-dist", "fonts", "/fonts", app);
            
            app.UseOwin(x => x.UseNancy(options =>
            {
                options.Bootstrapper = new Bootstrapper(container);
            }));
        }

        private void ConfigureStaticDirectoriesFor(string siteDirectory, string assetDirectory, string mappedPath, IApplicationBuilder app)
        {
            var current = AppDomain.CurrentDomain.BaseDirectory;
            var site = Path.Combine(current, siteDirectory);
            var asset = Path.Combine(site, assetDirectory);

            if (!Directory.Exists(site)) return;
            if (!Directory.Exists(asset)) return;

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(asset),
                RequestPath = mappedPath
            });
        }
    }
}
