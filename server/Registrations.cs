using Autofac;
using Solera.Server.Core.Composition;

namespace Solera.Server
{
    public class Registrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(MediationModule).Assembly);
        }
    }
}