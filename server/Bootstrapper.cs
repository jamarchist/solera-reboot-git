using Autofac;
using Nancy.Bootstrappers.Autofac;

namespace Solera.Server
{
    public class Bootstrapper : Nancy.Bootstrappers.Autofac.AutofacNancyBootstrapper
    {
        private readonly ILifetimeScope dotNetContainer;

        public Bootstrapper(ILifetimeScope dotNetContainer)
        {
            this.dotNetContainer = dotNetContainer;
        }

        protected override ILifetimeScope GetApplicationContainer()
        {
            return dotNetContainer;
        }
    }
}