using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Autofac;
using Autofac.Core;

namespace Solera.Server.Tests.Composition
{
    public class WithSettings : Module
    {
        private readonly IDictionary<string, string> settings;

        public WithSettings(IDictionary<string, string> settings)
        {
            this.settings = settings;
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            var potential = registration.Services.FirstOrDefault() as IServiceWithType;
            if (potential == null || potential.ServiceType != typeof(IConfigurationBuilder)) return;

            registration.Activating += (object sender, ActivatingEventArgs<object> e) =>
            {
                (e.Instance as IConfigurationBuilder).AddInMemoryCollection(settings);
            };            
        }        
    }
}