using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Autofac;
using Solera.Server.Core.Composition;
using Solera.Server.Tests.Conventions;
using Microsoft.Extensions.Configuration;

namespace Solera.Server.Tests.Composition
{
    public class ComposeIsolatedSystem : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // This is the whole system's default composition minus the ASP.NET stuff (and configuration)
            builder.RegisterAssemblyModules(typeof(MediationModule).Assembly);

            // This allows test-specific configuration
            builder.RegisterType<ConfigurationBuilder>().As<IConfigurationBuilder>().SingleInstance();
            builder.Register(c => c.Resolve<IConfigurationBuilder>().Build()).As<IConfiguration>().SingleInstance();
            builder.RegisterAssemblyTypes(ThisAssembly).Where(t => t.IsAssignableTo<ISetup>()).AsSelf().As<ISetup>();

            // This includes all of the commands and queries used by the tests and test infrastructure
            builder.RegisterMediationHandlers(ThisAssembly);

            // This provides some context for file system manipulation
            builder.RegisterModule(new WithSettings(new Dictionary<string, string>{
                {"Solution:WorkspaceRoot", CalculateWorkspaceDirectory()}
            }));

            // This is the log output
            builder.RegisterInstance(new StreamWriter(new MemoryStream())).As<StreamWriter>();
        }

        private string CalculateWorkspaceDirectory()
        {
            // It probably makes more sense to just pass this in somehow via command line
            // {workspaceRoot}/server-tests/bin/Debug/netcoreapp2.0
            var appDomainDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            return appDomainDirectory.Parent    // Debug
                                    .Parent     // bin
                                    .Parent     // server-tests
                                    .Parent     // {workspaceRoot}
                                    .FullName;
        }
    }
}