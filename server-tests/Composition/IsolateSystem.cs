using System.Collections.Generic;
using Autofac;

namespace Solera.Server.Tests.Composition
{
    public class IsolateSystem : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<ComposeIsolatedSystem>();
            builder.RegisterModule(new WithSettings(new Dictionary<string, string>{
                {"ConnectionStrings:MongoDB", "mongodb://localhost:27017/"}
            }));
        }
    }
}