using MongoDB.Driver;
using Solera.Server.Core.Migration;
using Solera.Server.Core.MigrationScripts._2019._04;
using Solera.Server.Core.Mongo;
using Solera.Server.Tests.Composition;
using Solera.Server.Tests.Conventions;
using Solera.Server.Tests.Setup;

namespace Solera.Server.Tests.InfrastructureTests
{
    public class when_applying_migrations : Scenario<IsolateSystem>,
        WithSetup<ServeCleanMongoDatabase>,
        WithSetup<ApplicationStartup>
    {
        private readonly IDatabase db;

        public when_applying_migrations(IDatabase db)
        {
            this.db = db;
        }

        public void the_first_migration_is_applied()
        {
            var versions = db.GetCollection<DatabaseVersion>();
            var firstVersion = versions.Find(x => x.ScriptName == typeof(_000_initialize_database).FullName).Single();
        }
    }
}