using System.Collections.Generic;
using Autofac;
using Solera.Server.Tests.Composition;
using Solera.Server.Tests.Conventions;
using Shouldly;

namespace Solera.Server.Tests.InfrastructureTests
{
    public class when_using_canned_setup_with_multiple_setups : Scenario<TestSystem>, 
        WithSetup<CannedSetupOne>,
        WithSetup<CannedSetupTwo>,
        WithSetup<CannedSetupThree>
    {
        private readonly IList<string> executed;

        public when_using_canned_setup_with_multiple_setups(IList<string> executed)
        {
            this.executed = executed;
        }

        public void setup_executes_in_order()
        {
            executed[0].ShouldBe("CannedSetupOne");
            executed[1].ShouldBe("CannedSetupTwo");
            executed[2].ShouldBe("CannedSetupThree");
        }
    }

    public class TestSystem : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var executed = new List<string>();
            builder.RegisterInstance(executed).As<IList<string>>();
            builder.RegisterModule<ComposeIsolatedSystem>();
        }
    }

    public class CannedSetupOne : CannedSetup
    {
        public CannedSetupOne(IList<string> executed) : base(executed)
        {
        }
    }

    public class CannedSetupTwo : CannedSetup
    {
        public CannedSetupTwo(IList<string> executed) : base(executed)
        {
        }
    }

    public class CannedSetupThree : CannedSetup
    {
        public CannedSetupThree(IList<string> executed) : base(executed)
        {
        }
    }

    public class CannedSetup : ISetup
    {
        private readonly IList<string> executed;

        public CannedSetup(IList<string> executed)
        {
            this.executed = executed;
        }

        public void CleanUp()
        {
            // pass
        }

        public void Execute()
        {
            this.executed.Add(this.GetType().Name);
        }
    }
}