using System;
using System.IO;
using Shouldly;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Mongo;
using Solera.Server.Tests.Composition;
using Solera.Server.Tests.Configuration;
using Solera.Server.Tests.Conventions;
using Solera.Server.Tests.Setup;
using Solera.Server.Tests.Teardown;
using MongoDB.Driver;

namespace Solera.Server.Tests.InfrastructureTests
{
    public class when_serving_mongo : Scenario<IsolateSystem>
    {
        private readonly ICommands commands;
        private readonly SolutionSettings config;
        private readonly Func<IDatabase> dbFactory;
        private readonly string dbPath;

        public when_serving_mongo(ICommands commands, SolutionSettings config, Func<IDatabase> dbFactory)
        {
            this.commands = commands;
            this.config = config;
            this.dbFactory = dbFactory;

            dbPath = Path.Combine(config.WorkspaceRoot, "server-tests", "database", "solera");
        }

        public override void Execute()
        {
            var toolPath = Path.Combine(config.WorkspaceRoot, "tools", "mongodb", "mongod.exe");

            commands.Execute(new CreateDirectory { Path = dbPath }).Wait();
            commands.Execute(new CleanDirectory { Path = dbPath }).Wait();
            commands.Execute(new ServeMongo { DatabasePath = dbPath, MongoExecutablePath = toolPath }).Wait();
        }

        public override void CleanUp()
        {
            commands.Execute(new StopMongo()).Wait();
            commands.Execute(new CleanDirectory { Path = dbPath }).Wait();
        }

        public void collection_names_are_pluralized()
        {
            var db = dbFactory();
            var collection = db.GetCollection<Widget>();
            collection.CollectionNamespace.CollectionName.ShouldBe("Widgets");
        }

        public class Widget { }
    }
}