using System;
using System.Collections.Generic;
using Autofac;
using Shouldly;
using Solera.Server.Tests.Composition;
using Solera.Server.Tests.Conventions;

namespace Solera.Server.Tests.InfrastructureTests
{
    public class SystemWithArbitrarySettingsSpecified : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<ComposeIsolatedSystem>();
            builder.RegisterModule(new WithSettings(new Dictionary<string, string>{
                { "Arbitrary:StringValue", "hello" },
                { "Arbitrary:IntegerValue", "123"}
            }));
        }
    }

    public class ArbitrarySettings
    {
        public string StringValue { get; set; }
        public int IntegerValue { get; set; }
    }

    public class when_resolving_arbitrary_settings : Scenario<SystemWithArbitrarySettingsSpecified>
    {
        private readonly ArbitrarySettings settings;

        public when_resolving_arbitrary_settings(ArbitrarySettings settings)
        {
            this.settings = settings;
        }

        public void the_settings_instance_is_not_null()
        {
            settings.ShouldNotBeNull();
        }

        public void the_string_value_is_correct()
        {
            settings.StringValue.ShouldBe("hello");
        }

        public void the_integer_value_is_correct()
        {
            settings.IntegerValue.ShouldBe(123);
        }
    }
}