using Autofac;

namespace Solera.Server.Tests.Conventions
{
    public interface IScenario
    {
        void Execute();
        void CleanUp();        
    }

    public interface IScenario<TModule> : IScenario where TModule : Module
    {

    }
}