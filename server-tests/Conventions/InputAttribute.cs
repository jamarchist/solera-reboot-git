using System;

namespace Solera.Server.Tests.Conventions
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class InputAttribute : Attribute
    {
        public object[] Parameters { get; }

        public InputAttribute(params object[] parameters)
        {
            this.Parameters = parameters;
        }
    }
}