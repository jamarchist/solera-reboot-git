using Autofac;

namespace Solera.Server.Tests.Conventions
{
    public class Scenario<TComposition> : IScenario<TComposition> where TComposition : Module
    {
        [Ignore]
        public virtual void Execute() { }
        [Ignore]
        public virtual void CleanUp() { }
    }
}