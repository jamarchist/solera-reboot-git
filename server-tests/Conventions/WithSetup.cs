namespace Solera.Server.Tests.Conventions
{
    public interface WithSetup<TSetup> where TSetup : ISetup { }
}