using System;
using System.Linq;
using System.Text;
using Autofac;
using Fixie;

namespace Solera.Server.Tests.Conventions
{
    public class ExecuteScenarios : Execution
    {
        public void Execute(TestClass testClass)
        {
            // Build the container for the scenario
            var iScenarioT = testClass.Type.GetInterfaces()
                .Where(t => t.IsClosedTypeOf(typeof(IScenario<>)))
                .Single();
            var setupClasses = testClass.Type.GetInterfaces()
                .Where(t => t.IsClosedTypeOf(typeof(WithSetup<>)))
                    .ToList()
                .Select(t => t.GenericTypeArguments.Single())
                    .ToList();
            var moduleType = iScenarioT.GenericTypeArguments.Single();

            var builder = new ContainerBuilder();
            builder.RegisterType(testClass.Type);
            builder.RegisterModule(Activator.CreateInstance(moduleType) as Module);

            var container = builder.Build();

            // Run the scenario
            var fixture = container.Resolve(testClass.Type) as IScenario;
            var setups = setupClasses.Select(s => container.Resolve(s) as ISetup).ToList();

            foreach (var setup in setups)
            {
                setup.Execute();
            }

            try
            {
                fixture.Execute();
                testClass.RunCases(@case =>
                {
                    @case.Execute(fixture);
                });

                try
                {
                    fixture.CleanUp();
                }
                catch (Exception cleanUpFailure)
                {
                    System.Console.WriteLine($"Failed to clean up '{testClass.Type.Name}'");
                    System.Console.WriteLine(cleanUpFailure.ToString());
                }                
            }
            finally
            {
                setups.Reverse();
                foreach (var setup in setups)
                {
                    try
                    {
                        setup.CleanUp();
                    }
                    catch (Exception cleanUpFailure)
                    {
                        System.Console.WriteLine($"Failed to clean up '{setup.GetType().Name}'");
                        System.Console.WriteLine(cleanUpFailure.ToString());
                    }
                }                
            }
        }
    }
}