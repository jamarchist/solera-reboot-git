using System;

namespace Solera.Server.Tests.Conventions
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class IgnoreAttribute : Attribute { }
}