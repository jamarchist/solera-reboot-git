using Autofac;
using Fixie;

namespace Solera.Server.Tests.Conventions
{
    public class DiscoverScenarios : Discovery
    {
        public DiscoverScenarios()
        {
            Classes
                .Where(t => !t.Has<IgnoreAttribute>())
                .Where(t => t.IsClosedTypeOf(typeof(Scenario<>)));

            Methods
                .Where(t => t.IsPublic)
                .Where(t => !t.Has<IgnoreAttribute>());

            Parameters
                .Add<FromInputAttributes>();
        }
    }
}