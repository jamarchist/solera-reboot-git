namespace Solera.Server.Tests.Conventions
{
    public interface ISetup
    {
        void Execute();
        void CleanUp();
    }
}