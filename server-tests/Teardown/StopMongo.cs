using System;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Diagnostics;
using Solera.Server.Core.Mediation;

namespace Solera.Server.Tests.Teardown
{
    public class StopMongo : ICommand { }

    public class StopMongoHandler : CommandHandler<StopMongo>
    {
        private readonly Func<string, IMongoDatabase> dbFactory;

        public StopMongoHandler(Func<string, IMongoDatabase> dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        protected override void Handle(StopMongo command)
        {
            var admin = dbFactory("admin");
            var shutdown = new ObjectCommand<BsonDocument>(new { shutdown = 1});

            try
            {
                var result = admin.RunCommand<BsonDocument>(shutdown);                
            }
            catch (MongoConnectionException shutdownError) 
            {
                // This happens because of the shutdown command
                System.Console.WriteLine("IGNORE THIS EXCEPTION");
                System.Console.WriteLine(shutdownError.Message);
                System.Console.WriteLine("-- END IGNORE --");
            }
        }
    }
}