using System;
using System.IO;
using Solera.Server.Core.Mediation;
using Solera.Server.Core.Mongo;
using Solera.Server.Tests.Composition;
using Solera.Server.Tests.Configuration;
using Solera.Server.Tests.Conventions;
using Solera.Server.Tests.Teardown;

namespace Solera.Server.Tests.Setup
{
    public class ServeCleanMongoDatabase : ISetup
    {
        private readonly ICommands commands;
        private readonly SolutionSettings config;
        private readonly Func<IDatabase> dbFactory;
        private readonly string dbPath;

        public ServeCleanMongoDatabase(ICommands commands, SolutionSettings config, Func<IDatabase> dbFactory)
        {
            this.commands = commands;
            this.config = config;
            this.dbFactory = dbFactory;

            dbPath = Path.Combine(config.WorkspaceRoot, "server-tests", "database", "solera");
        }

        public void Execute()
        {
            var toolPath = Path.Combine(config.WorkspaceRoot, "tools", "mongodb", "mongod.exe");

            commands.Execute(new CreateDirectory { Path = dbPath }).Wait();
            commands.Execute(new CleanDirectory { Path = dbPath }).Wait();
            commands.Execute(new ServeMongo { DatabasePath = dbPath, MongoExecutablePath = toolPath }).Wait();
        }

        public void CleanUp()
        {
            commands.Execute(new StopMongo()).Wait();
            commands.Execute(new CleanDirectory { Path = dbPath }).Wait();
        }
    }
}