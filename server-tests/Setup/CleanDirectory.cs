using System.IO;
using Solera.Server.Core.Mediation;

namespace Solera.Server.Tests.Setup
{
    public class CleanDirectory : ICommand { public string Path { get; set; } }

    public class CleanDirectoryHandler : CommandHandler<CleanDirectory>
    {
        protected override void Handle(CleanDirectory command)
        {            
            if (!Directory.Exists(command.Path)) return;

            var directory = new DirectoryInfo(command.Path);
            foreach (FileInfo file in directory.EnumerateFiles())
            {
                file.Delete(); 
            }
            foreach (DirectoryInfo subDirectory in directory.EnumerateDirectories())
            {
                subDirectory.Delete(true); 
            }
        }
    }
}