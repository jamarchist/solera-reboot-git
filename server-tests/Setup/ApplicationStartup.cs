using Solera.Server.Core.Mediation;
using Solera.Server.Core.Migration;
using Solera.Server.Tests.Conventions;

namespace Solera.Server.Tests.Setup
{
    public class ApplicationStartup : ISetup
    {
        private readonly ICommands commands;

        public ApplicationStartup(ICommands commands)
        {
            this.commands = commands;
        }

        public void Execute()
        {
            commands.Execute(new MigrateDatabase()).Wait();
        }

        public void CleanUp()
        {
            // pass
        }
    }
}