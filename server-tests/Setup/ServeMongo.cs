using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Solera.Server.Core.Mediation;
using Solera.Server.Tests.Configuration;

namespace Solera.Server.Tests.Setup
{
    public class ServeMongo : ICommand
    {
        public string DatabasePath { get; set; }
        public string MongoExecutablePath { get; set; }
    }


    public class ServeMongoHandler : CommandHandler<ServeMongo>
    {
        private readonly SolutionSettings config;
        private readonly StreamWriter log;

        public ServeMongoHandler(SolutionSettings config, StreamWriter log)
        {
            this.config = config;
            this.log = log;
        }

        protected override void Handle(ServeMongo command)
        {
            var mongoInfo = new ProcessStartInfo(command.MongoExecutablePath);
            mongoInfo.Arguments = $"--dbpath \"{command.DatabasePath}\" --directoryperdb";

            var mongo = new Process();
            mongo.StartInfo = mongoInfo;
            mongo.Start();

            // This just gives it a little time to startup
            System.Threading.Thread.Sleep(1000);
        }
    }
}