using System.IO;
using Solera.Server.Core.Mediation;

namespace Solera.Server.Tests.Setup
{
    public class CreateDirectory : ICommand { public string Path { get; set; } }

    public class CreateDirectoryHandler : CommandHandler<CreateDirectory>
    {
        protected override void Handle(CreateDirectory command)
        {
            if (!Directory.Exists(command.Path)) Directory.CreateDirectory(command.Path);
        }
    }
}