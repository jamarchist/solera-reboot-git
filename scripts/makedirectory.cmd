@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET me=%~n0
SET parent=%~dp0

SET newdir=%1

IF NOT EXIST %newdir% (
    ECHO [%me%] Attempting to create %newdir%.
    mkdir %newdir%
) ELSE (
    ECHO [%me%] Skipping creation of %newdir% as it already exists.
)