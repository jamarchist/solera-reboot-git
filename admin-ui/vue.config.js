const path = require("path");

const buildConfig = process.argv[3] || "--buildConfig=Debug";
const outSub = buildConfig.substring("--buildConfig=".length);

module.exports = {
        outputDir: path.resolve(__dirname, `./../output/${outSub}/admin-dist`),
        publicPath: '/admin/',
        configureWebpack: {
            devtool: 'source-map'
        }
};